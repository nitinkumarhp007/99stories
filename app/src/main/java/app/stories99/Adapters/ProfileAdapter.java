package app.stories99.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;

import app.stories99.Activities.StoryDetailActivity;
import app.stories99.Activities.StoryListingActivity;
import app.stories99.Model.CategoryModel;
import app.stories99.Model.StoryModel;
import app.stories99.R;


public class ProfileAdapter extends RecyclerView.Adapter<ProfileAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<StoryModel> list;

    public ProfileAdapter(Context context, ArrayList<StoryModel> list) {
        this.context = context;
        this.list = list;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public ProfileAdapter.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.profile_item, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ProfileAdapter.RecyclerViewHolder holder, final int position) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, StoryDetailActivity.class);
                intent.putExtra("story_id", list.get(position).getStory_id());
                context.startActivity(intent);
            }
        });
        if (list.get(position).getType().equals("2")) {
            holder.title.setText(list.get(position).getDescription());
        } else {
            holder.title.setText(list.get(position).getTitle());
        }

        Glide.with(context).load(list.get(position).getImage()).error(R.drawable.image_3).into(holder.image);


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private RoundedImageView image;
        private TextView title;

        public RecyclerViewHolder(View itemView) {
            super(itemView);

            image = (RoundedImageView) itemView.findViewById(R.id.image);
            title = (TextView) itemView.findViewById(R.id.title);

        }
    }
}
