package app.stories99.Adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import app.stories99.Model.BookExcerptsModel;
import app.stories99.R;
import app.stories99.parser.All99StoriesApis;
import app.stories99.util.GetAsync;
import app.stories99.util.Parameters;
import app.stories99.util.util;
import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.FormBody;
import okhttp3.RequestBody;


public class MyLibraryAdapter extends RecyclerView.Adapter<MyLibraryAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    ArrayList<BookExcerptsModel> list;
    boolean is_second = false;

    private View view;

    public MyLibraryAdapter(Context context, ArrayList<BookExcerptsModel> list) {
        this.context = context;
        this.list = list;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.my_libraray_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {
        if (list.get(position).getIs_completed().equals("1")) {
            holder.switchComplete.setChecked(true);
        } else {
            holder.switchComplete.setChecked(false);
        }
        holder.name.setText(list.get(position).getTitle());

        holder.switchComplete.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!is_second && position == list.size() - 1) {
                    is_second = true;
                } else {
                    COMPLETE_UNCOMPLETE_MY_BOOK_API(position);
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.switch_complete)
        SwitchCompat switchComplete;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private void COMPLETE_UNCOMPLETE_MY_BOOK_API(int position) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add(Parameters.BOOK_ID, list.get(position).getId());
        RequestBody formBody = formBuilder.build();
        GetAsync mAsync = new GetAsync(context, All99StoriesApis.COMPLETE_UNCOMPLETE_MY_BOOK_URL, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        Log.e("DATA", "data" + result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONObject jsonObjectBody = jsonmainObject.getJSONObject("body");
                        } else {

                            util.showToast(context, jsonmainObject.getString("message"));
                        }

                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }
}
