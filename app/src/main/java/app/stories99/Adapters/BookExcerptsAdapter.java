package app.stories99.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import app.stories99.Activities.BookExcerptDetailActivity;
import app.stories99.Activities.CommentActivity;
import app.stories99.Activities.StoryDetailActivity;
import app.stories99.Fragments.BookmarksFragment;
import app.stories99.Model.BookExcerptsModel;
import app.stories99.Model.StoryModel;
import app.stories99.R;
import butterknife.BindView;
import butterknife.ButterKnife;


public class BookExcerptsAdapter extends RecyclerView.Adapter<BookExcerptsAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    ArrayList<BookExcerptsModel> list;
    private View view;

    public BookExcerptsAdapter(Context context, ArrayList<BookExcerptsModel> list) {
        this.context = context;
        this.list = list;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.row_bookexcerpts, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        holder.name.setText(list.get(position).getTitle());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, BookExcerptDetailActivity.class);
                intent.putParcelableArrayListExtra("list", list);
                intent.putExtra("position", position);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
