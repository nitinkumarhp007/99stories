package app.stories99.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import app.stories99.Activities.StoryDetailActivity;
import app.stories99.Model.StoryModel;
import app.stories99.R;
import butterknife.BindView;
import butterknife.ButterKnife;


public class UserStorylistingAdapter extends RecyclerView.Adapter<UserStorylistingAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    private ArrayList<StoryModel> list;

    public UserStorylistingAdapter(Context context, ArrayList<StoryModel> list) {
        this.context = context;
        this.list = list;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.story_listing_item, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        if (list.get(position).getType().equals("2")) {
            holder.title.setText(list.get(position).getDescription());
        } else {
            holder.title.setText(list.get(position).getTitle());
        }


        Glide.with(context).load(list.get(position).getImage()).error(R.drawable.image_3).into(holder.image);

        holder.like.setText(list.get(position).getLikes_count());
        holder.shortDescription.setText(list.get(position).getSummary());
        holder.comment.setText(list.get(position).getComments_count());
        holder.authorName.setText("Author:" + list.get(position).getAuthor());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, StoryDetailActivity.class);
                intent.putExtra("story_id", list.get(position).getStory_id());
                context.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.short_description)
        TextView shortDescription;
        @BindView(R.id.like)
        TextView like;
        @BindView(R.id.comment)
        TextView comment;
        @BindView(R.id.author_name)
        TextView authorName;
        @BindView(R.id.bookmark)
        ImageView bookmark;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, view);

        }
    }
}
