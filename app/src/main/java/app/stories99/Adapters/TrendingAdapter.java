package app.stories99.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import app.stories99.Activities.CommentActivity;
import app.stories99.Activities.StoryDetailActivity;
import app.stories99.Fragments.TrendingFragment;
import app.stories99.Model.StoryModel;
import app.stories99.R;
import butterknife.BindView;
import butterknife.ButterKnife;


public class TrendingAdapter extends RecyclerView.Adapter<TrendingAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<StoryModel> list;
    TrendingFragment trendingFragment;
    public TrendingAdapter(Context context, ArrayList<StoryModel> list,TrendingFragment trendingFragment) {
        this.context = context;
        this.list = list;
        this.trendingFragment = trendingFragment;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.trending_item, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, StoryDetailActivity.class);
                intent.putExtra("story_id", list.get(position).getStory_id());
                context.startActivity(intent);
            }
        });

        if (list.get(position).getType().equals("2")) {
            holder.title.setText(list.get(position).getDescription());
        } else {
            holder.title.setText(list.get(position).getTitle());
        }

        holder.shortDescription.setText(list.get(position).getSummary());
        Glide.with(context).load(list.get(position).getImage()).error(R.drawable.image_3).into(holder.image);
        holder.like.setText(list.get(position).getLikes_count());
        holder.comment.setText(list.get(position).getComments_count());
        holder.authorName.setText("Author:" + list.get(position).getAuthor());


        holder.like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                trendingFragment.LIKE_DISLIKE_STORY_API(position);
            }
        });

        holder.comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, CommentActivity.class);
                i.putExtra("story_id", list.get(position).getStory_id());
                trendingFragment.startActivity(i);
            }
        });

        holder.bookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                trendingFragment.BOOKMARK_UNBOOKMARK_STORY_API(position);
            }
        });
        if (list.get(position).getIs_like().equals("1")) {
            Drawable img = context.getResources().getDrawable(R.drawable.like);
            holder.like.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
        } else {
            Drawable img = context.getResources().getDrawable(R.drawable.like);
            holder.like.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
        }
        if (list.get(position).getIs_bookmarked().equals("1")) {
            Drawable img = context.getResources().getDrawable(R.drawable.bookmark_2);
            holder.bookmark.setImageDrawable(img);
        } else {
            Drawable img = context.getResources().getDrawable(R.drawable.bookmark_1);
            holder.bookmark.setImageDrawable(img);
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.short_description)
        TextView shortDescription;
        @BindView(R.id.like)
        TextView like;
        @BindView(R.id.comment)
        TextView comment;
        @BindView(R.id.author_name)
        TextView authorName;
        @BindView(R.id.bookmark)
        ImageView bookmark;
        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,view);

        }
    }
}
