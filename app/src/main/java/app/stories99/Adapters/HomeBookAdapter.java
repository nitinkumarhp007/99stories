package app.stories99.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;

import app.stories99.Activities.BookExcerptsActivity;
import app.stories99.Activities.MyLibraryActivity;
import app.stories99.Activities.StoryListingActivity;
import app.stories99.Model.CategoryModel;
import app.stories99.R;


public class HomeBookAdapter extends RecyclerView.Adapter<HomeBookAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    int[] image;
    String[] title;
    String[] tempList;

    public HomeBookAdapter(Context context, int[] image, String[] name) {
        this.context = context;
        this.image = image;
        this.title = name;
        this.tempList = name;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public HomeBookAdapter.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.home_item, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final HomeBookAdapter.RecyclerViewHolder holder, final int position) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position == 0) {
                    Intent intent = new Intent(context, BookExcerptsActivity.class);
                    context.startActivity(intent);
                } else if (position == 1) {
                    Intent intent = new Intent(context, MyLibraryActivity.class);
                    context.startActivity(intent);
                } else if (position == 2) {

                }
            }
        });

        holder.title.setText(title[position]);
        Glide.with(context).load(image[position]).error(R.drawable.image_3).into(holder.image);


    }

    @Override
    public int getItemCount() {
        return image.length;
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private RoundedImageView image;
        private TextView title;

        public RecyclerViewHolder(View itemView) {
            super(itemView);

            image = (RoundedImageView) itemView.findViewById(R.id.image);
            title = (TextView) itemView.findViewById(R.id.title);

        }
    }

}
