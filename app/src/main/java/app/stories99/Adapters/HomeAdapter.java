package app.stories99.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;

import app.stories99.Activities.ForgotPasswordActivity;
import app.stories99.Activities.StoryListingActivity;
import app.stories99.Model.CategoryModel;
import app.stories99.R;


public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<CategoryModel> list;
    ArrayList<CategoryModel> tempList;

    public HomeAdapter(Context context, ArrayList<CategoryModel> list) {
        this.context = context;
        this.list = list;
        this.tempList = list;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public HomeAdapter.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.home_item, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final HomeAdapter.RecyclerViewHolder holder, final int position) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(context,StoryListingActivity.class);
                intent.putExtra("category_id",list.get(position).getCategory_id());
                intent.putExtra("category_name",list.get(position).getTitle());
                context.startActivity(intent);
            }
        });

        holder.title.setText(list.get(position).getTitle());
        Glide.with(context).load(list.get(position).getImage()).error(R.drawable.image_3).into(holder.image);


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private RoundedImageView image;
        private TextView title;

        public RecyclerViewHolder(View itemView) {
            super(itemView);

            image = (RoundedImageView) itemView.findViewById(R.id.image);
            title = (TextView) itemView.findViewById(R.id.title);

        }
    }
    public void filter(String charText) {
        charText = charText.toLowerCase();
        ArrayList<CategoryModel> nList = new ArrayList<CategoryModel>();
        if (charText.length() == 0) {
            nList.addAll(tempList);
        } else {
            for (CategoryModel wp : tempList) {
                String value = wp.getTitle();
                if (value.toLowerCase().contains(charText.toLowerCase()))//contains for less accurate result nd matches for accurate result
                {
                    nList.add(wp);
                }
            }
        }
        list = nList;
        notifyDataSetChanged();
    }
}
