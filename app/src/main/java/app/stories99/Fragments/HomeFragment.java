package app.stories99.Fragments;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import app.stories99.Activities.LoginActivity;
import app.stories99.Adapters.HomeAdapter;
import app.stories99.Adapters.HomeBookAdapter;
import app.stories99.Model.CategoryModel;
import app.stories99.R;
import app.stories99.parser.All99StoriesApis;
import app.stories99.util.GetAsyncGet;
import app.stories99.util.SavePref;
import app.stories99.util.util;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.FormBody;
import okhttp3.RequestBody;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {
    Context context;

    @BindView(R.id.search)
    EditText search;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    Unbinder unbinder;
    HomeAdapter adapter;
    ArrayList<CategoryModel> list;
    /*@BindView(R.id.books_recycler_view)
    RecyclerView booksRecyclerView;*/

    //int[] image = {R.drawable.recipe, R.drawable.recipe, R.drawable.recipe,};
   // String[] title = {"Book Excerts", "My Libaray", "Word Counter"};

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, view);
        context = getActivity();
        util.hideKeyboard(getActivity());
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        GET_ALL_CATEGORIES_API();
        return view;
    }

    private void GET_ALL_CATEGORIES_API() {
        SavePref savePref = new SavePref(context);
        Log.e("auth_key", savePref.getAUTH_KEY());
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, All99StoriesApis.GET_ALL_CATEGORIES_URL, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                list = new ArrayList<>();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        Log.e("DATA", "data" + result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONArray body = jsonmainObject.getJSONArray("body");

                            for (int i = 0; i < body.length(); i++) {
                                JSONObject jsonObject = body.getJSONObject(i);
                                CategoryModel categoryModel = new CategoryModel();
                                categoryModel.setCategory_id(jsonObject.getString("id"));
                                categoryModel.setImage(jsonObject.getString("image"));
                                categoryModel.setTitle(jsonObject.getString("title"));
                                list.add(categoryModel);
                            }
                            adapter = new HomeAdapter(context, list);
                            myRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2, GridLayoutManager.VERTICAL, false));
                            myRecyclerView.setAdapter(adapter);

                            //booksRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2, GridLayoutManager.VERTICAL, false));
                            //booksRecyclerView.setAdapter(new HomeBookAdapter(context, image, title));
                            //util.showToast(context, jsonmainObject.getString("message"));
                        } else {
                            if (util.Invalid_Authorization.equals(jsonmainObject.getString("message"))) {
                                SavePref savePref = new SavePref(context);
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            } else {
                                util.showToast(context, jsonmainObject.getString("message"));
                            }

                        }

                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


}
