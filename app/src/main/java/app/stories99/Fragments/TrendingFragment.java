package app.stories99.Fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import app.stories99.Activities.LoginActivity;
import app.stories99.Adapters.StorylistingAdapter;
import app.stories99.Adapters.TrendingAdapter;
import app.stories99.Model.StoryModel;
import app.stories99.R;
import app.stories99.parser.All99StoriesApis;
import app.stories99.util.GetAsync;
import app.stories99.util.GetAsyncGet;
import app.stories99.util.Parameters;
import app.stories99.util.SavePref;
import app.stories99.util.util;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.FormBody;
import okhttp3.RequestBody;

/**
 * A simple {@link Fragment} subclass.
 */
public class TrendingFragment extends Fragment {

    Context context;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    Unbinder unbinder;
    TrendingAdapter adapter;
    private ArrayList<StoryModel> list;

    public TrendingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_trending, container, false);
        unbinder = ButterKnife.bind(this, view);
        util.hideKeyboard(getActivity());
        context = getActivity();
        TRENDING_LISTINGAPI();
        return view;
    }

    private void TRENDING_LISTINGAPI() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        RequestBody formBody = formBuilder.build();
        GetAsyncGet mAsync = new GetAsyncGet(context, All99StoriesApis.TRENDING_LISTING_URL, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                list = new ArrayList<>();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        Log.e("DATA", "data" + result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONArray body = jsonmainObject.getJSONArray("body");

                            for (int i = 0; i < body.length(); i++) {
                                JSONObject jsonObject = body.getJSONObject(i);
                                StoryModel storyModel = new StoryModel();
                                storyModel.setStory_id(jsonObject.getString("story_id"));
                                storyModel.setImage(jsonObject.getString("story_image"));
                                storyModel.setTitle(jsonObject.getString("title"));
                                storyModel.setAuthor(jsonObject.getString("author"));
                                storyModel.setBookmarks_count(jsonObject.getString("bookmarks_count"));
                                storyModel.setComments_count(jsonObject.getString("comments_count"));
                                storyModel.setIs_bookmarked(jsonObject.getString("is_bookmarked"));
                                storyModel.setLikes_count(jsonObject.getString("likes_count"));
                                storyModel.setDescription(jsonObject.getString("description"));
                                storyModel.setIs_like(jsonObject.getString("is_liked"));
                                storyModel.setSummary(jsonObject.getString("summary"));
                                storyModel.setType(jsonObject.getString("type"));
                                list.add(storyModel);
                            }

                            adapter = new TrendingAdapter(context, list, TrendingFragment.this);
                            myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                            myRecyclerView.setAdapter(adapter);
                            //util.showToast(context, jsonmainObject.getString("message"));
                        } else {
                            if (util.Invalid_Authorization.equals(jsonmainObject.getString("message"))) {
                                SavePref savePref = new SavePref(context);
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);

                            } else {
                                util.showToast(context, jsonmainObject.getString("message"));
                            }
                        }

                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void LIKE_DISLIKE_STORY_API(final int position) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add(Parameters.STORY_ID, list.get(position).getStory_id());
        RequestBody formBody = formBuilder.build();
        GetAsync mAsync = new GetAsync(context, All99StoriesApis.LIKE_DISLIKE_STORY_URL, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        Log.e("DATA", "data" + result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            if (list.get(position).getIs_like().equals("1")) {
                                list.get(position).setIs_like("0");
                                list.get(position).setLikes_count(String.valueOf(Integer.parseInt(list.get(position).getLikes_count()) - 1));
                            } else {
                                list.get(position).setIs_like("1");
                                list.get(position).setLikes_count(String.valueOf(Integer.parseInt(list.get(position).getLikes_count()) + 1));
                            }
                            adapter.notifyDataSetChanged();
                        } else {
                            util.showToast(context, jsonmainObject.getString("message"));
                        }

                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void BOOKMARK_UNBOOKMARK_STORY_API(final int position) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add(Parameters.STORY_ID, list.get(position).getStory_id());
        RequestBody formBody = formBuilder.build();
        GetAsync mAsync = new GetAsync(context, All99StoriesApis.BOOKMARK_UNBOOKMARK_STORY_URL, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        Log.e("DATA", "data" + result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            if (list.get(position).getIs_bookmarked().equals("1")) {
                                list.get(position).setIs_bookmarked("0");
                                list.get(position).setBookmarks_count(String.valueOf(Integer.parseInt(list.get(position).getBookmarks_count()) - 1));
                            } else {
                                list.get(position).setIs_bookmarked("1");
                                list.get(position).setBookmarks_count(String.valueOf(Integer.parseInt(list.get(position).getBookmarks_count()) + 1));
                            }
                            adapter.notifyDataSetChanged();
                        } else {
                            util.showToast(context, jsonmainObject.getString("message"));
                        }

                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
