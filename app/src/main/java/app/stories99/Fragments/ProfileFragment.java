package app.stories99.Fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import app.stories99.Activities.AddStoryActivity;
import app.stories99.Activities.AddStoryParentActivity;
import app.stories99.Activities.LoginActivity;
import app.stories99.Activities.SettingActivity;
import app.stories99.Adapters.ProfileAdapter;
import app.stories99.Model.StoryModel;
import app.stories99.R;
import app.stories99.parser.All99StoriesApis;
import app.stories99.util.GetAsyncGet;
import app.stories99.util.SavePref;
import app.stories99.util.util;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.FormBody;
import okhttp3.RequestBody;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {
    Context context;
    @BindView(R.id.profilepic)
    ImageView profilepic;
    @BindView(R.id.username)
    TextView username;
    @BindView(R.id.story_count)
    TextView storyCount;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    Unbinder unbinder;
    @BindView(R.id.add_story)
    ImageView addStory;
    @BindView(R.id.setting)
    ImageView setting;

    private ArrayList<StoryModel> list;

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        unbinder = ButterKnife.bind(this, view);
        util.hideKeyboard(getActivity());
        context = getActivity();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        GET_USER_PROFILE_API();
    }

    private void GET_USER_PROFILE_API() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        RequestBody formBody = formBuilder.build();
        GetAsyncGet mAsync = new GetAsyncGet(context, All99StoriesApis.GET_USER_PROFILE_URL, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                list = new ArrayList<>();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        Log.e("DATA", "data" + result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONObject body = jsonmainObject.getJSONObject("body");

                            username.setText(body.getString("name"));
                            storyCount.setText(body.getString("stories_count"));

                            Glide.with(context).load(body.getString("profile_photo")).error(R.drawable.profile).into(profilepic);
                            JSONArray stories = body.getJSONArray("stories");
                            for (int i = 0; i < stories.length(); i++) {
                                JSONObject jsonObject = stories.getJSONObject(i);
                                StoryModel storyModel = new StoryModel();
                                storyModel.setStory_id(jsonObject.getString("id"));
                                storyModel.setImage(jsonObject.getString("image"));
                                storyModel.setTitle(jsonObject.getString("title"));
                                storyModel.setType(jsonObject.getString("type"));
                                storyModel.setDescription(jsonObject.getString("description"));
                             /*   storyModel.setAuthor(jsonObject.getString("author"));
                                storyModel.setBookmarks_count(jsonObject.getString("bookmarks_count"));
                                storyModel.setComments_count(jsonObject.getString("comments_count"));
                                storyModel.setIs_bookmarked(jsonObject.getString("is_bookmarked"));
                                storyModel.setLikes_count(jsonObject.getString("likes_count"));*/
                                list.add(storyModel);
                            }
                            ProfileAdapter adapter = new ProfileAdapter(context, list);
                            myRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2, GridLayoutManager.VERTICAL, false));
                            myRecyclerView.setAdapter(adapter);
                            //util.showToast(context, jsonmainObject.getString("message"));
                        } else {
                            if (util.Invalid_Authorization.equals(jsonmainObject.getString("message"))) {
                                SavePref savePref = new SavePref(context);
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);

                            } else {
                                util.showToast(context, jsonmainObject.getString("message"));
                            }
                        }

                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick({R.id.setting, R.id.add_story})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.setting:
                Intent i = new Intent(context, SettingActivity.class);
                startActivity(i);
                break;
            case R.id.add_story:
                Intent intent = new Intent(context, AddStoryParentActivity.class);
                startActivity(intent);
                break;
        }
    }
}
