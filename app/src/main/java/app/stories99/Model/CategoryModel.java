package app.stories99.Model;

/**
 * Created by Sushant on 11/24/2018.
 */

public class CategoryModel {

    String category_id="";
    String title="";
    String image="";

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
