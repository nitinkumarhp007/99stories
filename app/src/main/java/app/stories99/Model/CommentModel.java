package app.stories99.Model;

/**
 * Created by Sushant on 11/25/2018.
 */

public class CommentModel {
    String id="";
    String comment="";
    String created="";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }
}
