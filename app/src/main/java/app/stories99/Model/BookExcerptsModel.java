package app.stories99.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Sushant on 12/24/2018.
 */

public class BookExcerptsModel implements Parcelable {
    String id = "";
    String title = "";
    String link = "";
    String description = "";
    String is_completed = "";

    public BookExcerptsModel() {

    }

    protected BookExcerptsModel(Parcel in) {
        id = in.readString();
        title = in.readString();
        link = in.readString();
        description = in.readString();
        is_completed = in.readString();
    }

    public static final Creator<BookExcerptsModel> CREATOR = new Creator<BookExcerptsModel>() {
        @Override
        public BookExcerptsModel createFromParcel(Parcel in) {
            return new BookExcerptsModel(in);
        }

        @Override
        public BookExcerptsModel[] newArray(int size) {
            return new BookExcerptsModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public String getIs_completed() {
        return is_completed;
    }

    public void setIs_completed(String is_completed) {
        this.is_completed = is_completed;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(title);
        dest.writeString(link);
        dest.writeString(description);
        dest.writeString(is_completed);
    }
}


