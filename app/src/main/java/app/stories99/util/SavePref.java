package app.stories99.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by android on 14/3/17.
 */

public class SavePref {
    public static final String TAG = "SavePref";
    Context context;
    public static final String PREF_TOKEN = "EXPRESS_TOKEN";
    public static final String CHAT_SCREEN = "chat_screen";
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    public SavePref(Context c) {
        context = c;
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        editor = preferences.edit();

    }

    public void setUserId(String user_id) {
        editor.putString("user_id", user_id);
        editor.commit();
    }

    public String getUserId() {
        String id = preferences.getString("user_id", "");
        return id;
    }

    public void setName(String name) {
        editor.putString("name", name);
        editor.commit();
    }



    public String getName() {
        String name = preferences.getString("name", "");
        return name;
    }

    public void setcity_id(String city_id) {
        editor.putString("city_id", city_id);
        editor.commit();
    }

    public String getcity_id() {
        String city_id = preferences.getString("city_id", "");
        return city_id;
    }
    public void setcity_name(String city_name) {
        editor.putString("city_name", city_name);
        editor.commit();
    }

    public String getcity_name() {
        String city_name = preferences.getString("city_name", "");
        return city_name;
    }

    public void setCountry_id(String Country_id) {
        editor.putString("Country_id", Country_id);
        editor.commit();
    }

    public String getCountry_id() {
        String Country_id = preferences.getString("Country_id", "");
        return Country_id;
    }

    public void setcounty_name(String county_name) {
        editor.putString("county_name", county_name);
        editor.commit();
    }

    public String getcounty_name() {
        String county_name = preferences.getString("county_name", "");
        return county_name;
    }

    public void setneighbourhoodName(String neighbourhoodName) {
        editor.putString("neighbourhoodName", neighbourhoodName);
        editor.commit();
    }

    public String getneighbourhoodName() {
        String neighbourhoodName = preferences.getString("neighbourhoodName", "");
        return neighbourhoodName;
    }

    public void setneighbourhood_id(String neighbourhood_id) {
        editor.putString("neighbourhood_id", neighbourhood_id);
        editor.commit();
    }

    public String getneighbourhood_id() {
        String neighbourhood_id = preferences.getString("neighbourhood_id", "");
        return neighbourhood_id;
    }

    public void setSelectedLanguage(String selectedLanguage) {
        editor.putString("selectedLanguage", selectedLanguage);
        editor.commit();
    }

    public String getSelectedLanguage() {
        String selectedLanguage = preferences.getString("selectedLanguage", "");
        return selectedLanguage;
    }

    public void setChatName(String chatName) {
        editor.putString("chatName", chatName);
        editor.commit();
    }

    public String getChatName() {
        String chatName = preferences.getString("chatName", "");
        return chatName;
    }

    public void setChatTime(String chatTime) {
        editor.putString("chatTime", chatTime);
        editor.commit();
    }

    public String getChatTime() {
        String chatTime = preferences.getString("chatTime", "");
        return chatTime;
    }


    public void setResponders_id(String respondersId) {
        editor.putString("respondersId", respondersId);
        editor.commit();
    }

    public String getResponders_id() {
        String respondersId = preferences.getString("respondersId", "");
        return respondersId;
    }

    public void isChatScreen(boolean value) {
        editor.putBoolean(CHAT_SCREEN, value);
        editor.commit();
    }

    public boolean getChatScreen() {
        return preferences.getBoolean(CHAT_SCREEN, false);
    }

    public void setCHAT_ID(String chat_id) {
        editor.putString("chat_id", chat_id);
        editor.commit();
    }

    public String getCHAT_ID() {
        String chat_id = preferences.getString("chat_id", "");
        return chat_id;
    }

    public void setUserName(String userName) {
        editor.putString("userName", userName);
        editor.commit();
    }

    public String getUserName() {
        String userName = preferences.getString("userName", "");
        return userName;
    }

    public void setPhone(String phone) {
        editor.putString("phone", phone);
        editor.commit();
    }

    public String getPhone() {
        String phone = preferences.getString("phone", "");
        return phone;
    }
    public void setblock_user_count(String block_user_count) {
        editor.putString("block_user_count", block_user_count);
        editor.commit();
    }

    public String getblock_user_count() {
        String block_user_count = preferences.getString("block_user_count", "");
        return block_user_count;
    }

    public void setaccount_notification(String account_notification) {
        editor.putString("account_notification", account_notification);
        editor.commit();
    }

    public String getaccount_notification() {
        String account_notification = preferences.getString("account_notification", "");
        return account_notification;
    }

    public void setnotification_on_ads_interaction(String notification_on_ads_interaction) {
        editor.putString("notification_on_ads_interaction", notification_on_ads_interaction);
        editor.commit();
    }

    public String getnotification_on_ads_interaction() {
        String notification_on_ads_interaction = preferences.getString("notification_on_ads_interaction", "");
        return notification_on_ads_interaction;
    }

    public void setadd_aded_notification(String add_aded_notification) {
        editor.putString("add_aded_notification", add_aded_notification);
        editor.commit();
    }

    public String getadd_aded_notification() {
        String add_aded_notification = preferences.getString("add_aded_notification", "");
        return add_aded_notification;
    }

    public void setAUTH_KEY(String auth_key) {
        editor.putString("auth_key", auth_key);
        editor.commit();
    }

    public String getAUTH_KEY() {
        String auth_key = preferences.getString("auth_key", "");
        return auth_key;
    }

    public void setDOB(String dob) {
        editor.putString("dob", dob);
        editor.commit();
    }

    public String getDOB() {
        String dob = preferences.getString("dob", "");
        return dob;
    }

    public void setProfile_pic(String profile_pic) {
        editor.putString("profile_pic", profile_pic);
        editor.commit();
    }

    public String getProfile_pic() {
        String profile_pic = preferences.getString("profile_pic", "");
        return profile_pic;
    }
    public void setNoti_icon_Count(String noti_icon_count) {
        editor.putString("noti_icon_count", noti_icon_count);
        editor.commit();
    }

    public String getNoti_icon_Count() {
        String noti_icon_count = preferences.getString("noti_icon_count", "");
        return noti_icon_count;
    }

    public void setEmail(String email) {
        editor.putString("email", email);
        editor.commit();
    }

    public String getEmail() {
        String email = preferences.getString("email", "");
        return email;
    }
    public void setNotifyCount(int notifyCount) {
        editor.putInt("notifyCount", notifyCount);
        editor.commit();
    }

    public int getNotifyCount() {
        int notifyCount = preferences.getInt("notifyCount", 0);
        return notifyCount;
    }

    public void setGender(String gender) {
        editor.putString("gender", gender);
        editor.commit();
    }

    public String getGender() {
        String gender = preferences.getString("gender", "");
        return gender;
    }
    public void setFlagImage(String flagimage) {
        editor.putString("flagimage", flagimage);
        editor.commit();
    }

    public String getFlagImage() {
        String flagimage = preferences.getString("flagimage", "");
        return flagimage;
    }
    public void setLoginType(String loginType) {
        editor.putString("loginType", loginType);
        editor.commit();
    }

    public String getLoginType() {
        String loginType = preferences.getString("loginType", "");
        return loginType;
    }


    public static void setDeviceToken(Context mContext, String key, String value) {
        SharedPreferences sharedpreferences = mContext.getSharedPreferences(PREF_TOKEN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String getDeviceToken(Context mContext, String key) {
        SharedPreferences preferences = mContext.getSharedPreferences(PREF_TOKEN, Context.MODE_PRIVATE);
        String stringvalue = preferences.getString(key, null);
        return stringvalue;
    }

    public static void setString(Context mContext, String key, String value) {
        SharedPreferences sharedpreferences = mContext.getSharedPreferences(PREF_TOKEN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String getString(Context mContext, String key, String def_value) {
        SharedPreferences preferences = mContext.getSharedPreferences(PREF_TOKEN, Context.MODE_PRIVATE);
        String stringvalue = preferences.getString(key, def_value);
        return stringvalue;
    }



    public void clearPreferences() {
        preferences.edit().clear().commit();
    }


}
