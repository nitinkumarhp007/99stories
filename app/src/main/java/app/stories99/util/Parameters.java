package app.stories99.util;

public class Parameters {
    public static final String DEVICE_TYPE = "device_type";
    public static final String DEVICE_TOKEN = "device_token";
    public static final String AUTHORIZATION_KEY = "Authorization-key";
    public static final String EMAIL_OR_USERNAME = "email_or_username";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String OLD_PASSWORD = "old_password";
    public static final String NEW_PASSWORD = "new_password";
    public static final String NAME = "name";
    public static final String USERNAME = "username";
    public static final String PROFILE_PHOTO = "profile_photo";
    public static final String CATEGORY_ID = "category_id";
    public static final String TITLE = "title";
    public static final String DESCRIPTION = "description";
    public static final String SUMMARY = "summary";
    public static final String SOCIAL_ID = "social_id";
    public static final String SOCIAL_TYPE = "social_type";
    public static final String STORY_ID = "story_id";
    public static final String COMMENT = "comment";
    public static final String TYPE = "type";
    public static final String USER_ID = "user_id";
    public static final String BOOK_ID = "book_id";
    public static final String REASON = "reason";
}
