package app.stories99;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.app.FragmentTabHost;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

import app.stories99.Containers.BookmarksContainer;
import app.stories99.Containers.Home_Container;
import app.stories99.Containers.ProfileContainer;
import app.stories99.Containers.RecomendedContainer;
import app.stories99.Containers.TrendingContainer;

public class MainActivity extends AppCompatActivity {
    MainActivity context;
    public static final String TAB_1_TAG = "tab_1";
    public static final String TAB_2_TAG = "tab_2";
    public static final String TAB_3_TAG = "tab_3";
    public static final String TAB_4_TAG = "tab_4";
    public static final String TAB_5_TAG = "tab_5";
    public static FragmentTabHost mFragmentTabHost;
    private LayoutInflater inflater = null;

    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = MainActivity.this;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mFragmentTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
        mFragmentTabHost.setup(context, context.getSupportFragmentManager(), R.id.realtabcontent);
        //inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View viewPeople = prepareTabView(R.drawable.tab_background_for_home, "Home");
        TabHost.TabSpec tabSpec1 = mFragmentTabHost.newTabSpec(TAB_1_TAG).setIndicator(viewPeople);
        mFragmentTabHost.addTab(tabSpec1, Home_Container.class, null);

        View viewBehaviour = prepareTabView(R.drawable.tab_background_for_trending, "Trending");
        TabHost.TabSpec tabSpec2 = mFragmentTabHost.newTabSpec(TAB_2_TAG).setIndicator(viewBehaviour);
        mFragmentTabHost.addTab(tabSpec2, TrendingContainer.class, null);

        View viewTimeTable = prepareTabView(R.drawable.tab_background_for_bookmark, "Bookmarks");
        TabHost.TabSpec tabSpec3 = mFragmentTabHost.newTabSpec(TAB_3_TAG).setIndicator(viewTimeTable);
        mFragmentTabHost.addTab(tabSpec3, BookmarksContainer.class, null);

        View vie = prepareTabView(R.drawable.tab_background_for_latest, "Latest");
        TabHost.TabSpec tabSpec4 = mFragmentTabHost.newTabSpec(TAB_4_TAG).setIndicator(vie);
        mFragmentTabHost.addTab(tabSpec4, RecomendedContainer.class, null);

        View viewTable = prepareTabView(R.drawable.tab_background_for_profile, "Profile");
        TabHost.TabSpec tabSpec5 = mFragmentTabHost.newTabSpec(TAB_5_TAG).setIndicator(viewTable);
        mFragmentTabHost.addTab(tabSpec5, ProfileContainer.class, null);


        mFragmentTabHost.setCurrentTab(0);
        mFragmentTabHost.getTabWidget().setDividerDrawable(null);

        mAdView = (AdView) findViewById(R.id.adView);


        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                // Check the LogCat to get your test device ID
                .addTestDevice("C04B1BFFB0774708339BC273F8A43708")
                .build();

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
            }

            @Override
            public void onAdClosed() {
               // Toast.makeText(getApplicationContext(), "Ad is closed!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                //Toast.makeText(getApplicationContext(), "Ad failed to load! error code: " + errorCode, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdLeftApplication() {
               // Toast.makeText(getApplicationContext(), "Ad left application!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
            }
        });

        mAdView.loadAd(adRequest);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }

    /***Customization For tabs***/
    @SuppressLint("InflateParams")
    private View prepareTabView(int drawable, String text) {
        View view = inflater.inflate(R.layout.layout_tab, null);
        ImageView image = (ImageView) view.findViewById(R.id.tab_text);
        TextView tab_text = (TextView) view.findViewById(R.id.tab_text1);
        tab_text.setText(text);
        image.setBackgroundResource(drawable);
        return view;
    }
}
