package app.stories99.Containers;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import app.stories99.R;


/**
 * Created by root on 24/4/18.
 */

public class Fragment_Container extends Fragment
{
    public void replaceFragment(Fragment fragment, boolean addToBackStack)
    {
        if (!isAdded()) return;

        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();

        if (addToBackStack) {
            transaction.addToBackStack(null);
        }

        transaction.replace(R.id.frame_layout_for_container, fragment);
        transaction.commit();
        getChildFragmentManager().executePendingTransactions();
    }

    public boolean popFragment() {
        Log.e("test", "pop fragment: " + getChildFragmentManager().getBackStackEntryCount());
        boolean isPop = false;
        if (getChildFragmentManager().getBackStackEntryCount() > 0)
        {
            isPop = true;
            getChildFragmentManager().popBackStack();
        }
        return isPop;
    }

}
