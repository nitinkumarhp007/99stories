package app.stories99.parser;


public class All99StoriesApis {
    public static final String BASE_URL = "http://18.225.16.217/99_stories/apis/";

    public static final String LOGIN_URL = BASE_URL + "login";
    public static final String SIGNUP_URL = BASE_URL + "signup";
    public static final String POST_USER_STORY_URL = BASE_URL + "post_user_story";
    public static final String CHANGE_PASSWORD_URL = BASE_URL + "change_password";
    public static final String LOGOUT_URL = BASE_URL + "logout";
    public static final String SOCIAL_LOGIN_URL = BASE_URL + "social_login";
    public static final String FORGOT_PASSWORD_URL = BASE_URL + "forgot_password";
    public static final String GET_ALL_CATEGORIES_URL = BASE_URL + "get_all_categories";
    public static final String GET_CATEGORY_STORY_URL = BASE_URL + "get_category_stories";
    public static final String TRENDING_LISTING_URL = BASE_URL + "trendings_listing";
    public static final String MY_BOOKMARKS_URL = BASE_URL + "my_bookmarks";
    public static final String RECOMMENDED_LISTINGURL = BASE_URL + "recommended_listing";
    public static final String GET_USER_PROFILE_URL = BASE_URL + "get_user_profile";
    public static final String VIEW_USER_STORY_URL = BASE_URL + "view_user_story";
    public static final String DELETE_USER_STORY_URL = BASE_URL + "delete_user_story";
    public static final String LIKE_DISLIKE_STORY_URL = BASE_URL + "like_dislike_story";
    public static final String BOOKMARK_UNBOOKMARK_STORY_URL = BASE_URL + "bookmark_unbookmark_story";
    public static final String COMMENT_STORY_URL = BASE_URL + "comment_story";
    public static final String STORY_COMMENTS_STORY_URL = BASE_URL + "story_comments_listing";
    public static final String USER_STORIES_URL = BASE_URL + "user_stories";
    public static final String TERM_AND_CONDITIONS_URL = BASE_URL + "terms_and_conditions";
    public static final String ABOUT_US_URL = BASE_URL + "about_us";
    public static final String MY_BOOK_LISTING_URL = BASE_URL + "my_book_listing";
    public static final String EXCERPTS_LISTING_URL = BASE_URL + "excerpts_listing";
    public static final String COMPLETE_UNCOMPLETE_MY_BOOK_URL = BASE_URL + "complete_uncomplete_my_book";
    public static final String ADD_BOOK_URL = BASE_URL + "add_book";
    public static final String REPORT_STORY = BASE_URL + "report_story";

}
