package app.stories99.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import app.stories99.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddStoryParentActivity extends AppCompatActivity {

    @BindView(R.id.Add_a_story)
    Button AddAStory;
    @BindView(R.id.Add_one_liner)
    Button AddOneLiner;
    @BindView(R.id.Add_a_poem)
    Button AddAPoem;
    @BindView(R.id.Add_a_picture)
    Button AddAPicture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_story_parent);
        ButterKnife.bind(this);
        setToolbar();
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText("Add");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @OnClick({R.id.Add_a_story, R.id.Add_one_liner, R.id.Add_a_poem, R.id.Add_a_picture})
    public void onViewClicked(View view) {
        Intent intent = new Intent(AddStoryParentActivity.this, AddStoryActivity.class);
        switch (view.getId()) {
            case R.id.Add_a_story:
                intent.putExtra("type", "0");
                intent.putExtra("title", AddAStory.getText().toString());
                break;
            case R.id.Add_one_liner:
                intent.putExtra("type", "1");
                intent.putExtra("title", AddOneLiner.getText().toString());
                break;
            case R.id.Add_a_poem:
                intent.putExtra("type", "2");
                intent.putExtra("title", AddAPoem.getText().toString());
                break;
            case R.id.Add_a_picture:
                intent.putExtra("type", "3");
                intent.putExtra("title", AddAPicture.getText().toString());
                break;
        }
        startActivityForResult(intent, 200);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 200) {
            finish();
        }
    }
}
