package app.stories99.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;


import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import app.stories99.R;
import app.stories99.parser.All99StoriesApis;
import app.stories99.util.ConnectivityReceiver;
import app.stories99.util.GetAsync;
import app.stories99.util.Parameters;
import app.stories99.util.SavePref;
import app.stories99.util.util;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class StoryDetailActivity extends AppCompatActivity {
    StoryDetailActivity context;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.delete)
    ImageView delete;
    @BindView(R.id.font_option)
    ImageView font_option;
    @BindView(R.id.edit)
    ImageView edit;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.author_name)
    TextView authorName;
    @BindView(R.id.description)
    TextView description;
    @BindView(R.id.like)
    TextView like;
    @BindView(R.id.comment)
    TextView comment;
    @BindView(R.id.share)
    TextView share;
    @BindView(R.id.bookmark)
    TextView bookmark;
    String story_id = "", author_id = "", story_image = "", description_ = "", summary_ = "",
            category_title = "", category_id = "", bookmarks_count = "", is_bookmarked = "", likes_count = "", title_ = "", is_liked = "";
    @BindView(R.id.main_layout)
    RelativeLayout mainLayout;

    SavePref savePref;
    String type = "";
    @BindView(R.id.seek_bar)
    SeekBar seekBar;
    @BindView(R.id.switch_nightmode)
    SwitchCompat switchNightmode;
    @BindView(R.id.nightmode_layout)
    LinearLayout nightmodeLayout;
    @BindView(R.id.bottom_sheet)
    LinearLayout bottomSheet;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    @BindView(R.id.report)
    ImageView report;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_story_detail);
        ButterKnife.bind(this);
        context = StoryDetailActivity.this;
        savePref = new SavePref(context);
        story_id = getIntent().getStringExtra("story_id");

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (progress > 10 && progress < 28) {
                    title.setTextSize(TypedValue.COMPLEX_UNIT_SP, progress + 1);
                    authorName.setTextSize(TypedValue.COMPLEX_UNIT_SP, progress + 1);
                    description.setTextSize(TypedValue.COMPLEX_UNIT_SP, progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        switchNightmode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mainLayout.setBackgroundColor(getResources().getColor(R.color.black));
                    title.setTextColor(getResources().getColor(R.color.white));
                    authorName.setTextColor(getResources().getColor(R.color.white));
                    description.setTextColor(getResources().getColor(R.color.white));
                } else {
                    mainLayout.setBackgroundColor(getResources().getColor(R.color.white));
                    title.setTextColor(getResources().getColor(R.color.black));
                    authorName.setTextColor(getResources().getColor(R.color.black));
                    description.setTextColor(getResources().getColor(R.color.black));
                }
            }
        });


    }

    private void VIEW_USER_STORY_API() {
        mainLayout.setVisibility(View.GONE);
        scrollView.setVisibility(View.GONE);
        bottomSheet.setVisibility(View.GONE);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add(Parameters.STORY_ID, story_id);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, All99StoriesApis.VIEW_USER_STORY_URL, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();

                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        Log.e("DATA", "data" + result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONObject body = jsonmainObject.getJSONObject("body");
                            title.setText(body.getString("title"));

                            if (body.getString("is_adult").equals("1")) {
                                Alert();
                            } else {
                                mainLayout.setVisibility(View.VISIBLE);
                                scrollView.setVisibility(View.VISIBLE);
                                bottomSheet.setVisibility(View.VISIBLE);
                            }


                            authorName.setText("Author:" + body.getString("author"));
                            description.setText(body.getString("description"));
                            like.setText(body.getString("likes_count"));
                            comment.setText(body.getString("comments_count"));
                            bookmark.setText(body.getString("bookmarks_count"));
                            Glide.with(context).load(body.getString("story_image")).into(image);
                            type = body.getString("type");

                            author_id = body.getString("author_id");
                            if (author_id.equals(savePref.getUserId())) {
                                delete.setVisibility(View.VISIBLE);
                                edit.setVisibility(View.VISIBLE);
                                report.setVisibility(View.GONE);
                            } else {
                                report.setVisibility(View.VISIBLE);
                                delete.setVisibility(View.GONE);
                                edit.setVisibility(View.GONE);
                            }

                            story_image = body.getString("story_image");
                            description_ = body.getString("description");
                            summary_ = body.getString("summary");
                            category_title = body.getString("category_title");
                            category_id = body.getString("category_id");
                            title_ = body.getString("title");
                            is_liked = body.getString("is_liked");
                            is_bookmarked = body.getString("is_bookmarked");
                            bookmarks_count = body.getString("bookmarks_count");
                            likes_count = body.getString("is_liked");

                            if (body.getString("is_liked").equals("1")) {
                                Drawable img = getResources().getDrawable(R.drawable.like);
                                like.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                            } else {
                                Drawable img = getResources().getDrawable(R.drawable.like);
                                like.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                            }
                            if (body.getString("is_bookmarked").equals("1")) {
                                Drawable img = getResources().getDrawable(R.drawable.bookmark_2);
                                bookmark.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                            } else {
                                Drawable img = getResources().getDrawable(R.drawable.bookmark_1);
                                bookmark.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                            }


                            // util.showToast(context, jsonmainObject.getString("message"));
                        } else {
                            if (util.Invalid_Authorization.equals(jsonmainObject.getString("message"))) {
                                SavePref savePref = new SavePref(context);
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);

                            } else {

                                util.showToast(context, jsonmainObject.getString("message"));
                            }
                        }

                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void Alert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setTitle(R.string.warning);
        builder.setMessage("This Content might not be suitable for all ages, Click YES to confirm you are 18 years and above")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mainLayout.setVisibility(View.VISIBLE);
                        scrollView.setVisibility(View.VISIBLE);
                        bottomSheet.setVisibility(View.VISIBLE);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        finish();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }


    @OnClick({R.id.report, R.id.back, R.id.font_option, R.id.delete, R.id.edit, R.id.author_name, R.id.like, R.id.comment, R.id.share, R.id.bookmark})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.font_option:
                if (nightmodeLayout.getVisibility() == View.GONE)
                    nightmodeLayout.setVisibility(View.VISIBLE);
                else
                    nightmodeLayout.setVisibility(View.GONE);
                break;
            case R.id.back:
                finish();
                break;
            case R.id.delete:
                DeleteAlert();
                break;
            case R.id.report:
                Intent intentaaa = new Intent(context, ReportStoryActivity.class);
                intentaaa.putExtra("story_id", story_id);
                startActivity(intentaaa);
                break;
            case R.id.edit:
                Intent intent = new Intent(context, AddStoryActivity.class);
                intent.putExtra("story_id", story_id);
                intent.putExtra("title", title_);
                intent.putExtra("story_image", story_image);
                intent.putExtra("category_id", category_id);
                intent.putExtra("category_title", category_title);
                intent.putExtra("description", description_);
                intent.putExtra("summary", summary_);
                intent.putExtra("is_edit", true);
                intent.putExtra("type", type);
                startActivity(intent);
                break;
            case R.id.author_name:
                Intent i1 = new Intent(context, UserStoriesActivity.class);
                i1.putExtra("user_id", author_id);
                startActivity(i1);
                break;
            case R.id.like:
                LIKE_DISLIKE_STORY_API();
                break;
            case R.id.comment:
                Intent i = new Intent(context, CommentActivity.class);
                i.putExtra("story_id", story_id);
                startActivity(i);
                break;
            case R.id.share:
                Intent sendIntent = new Intent(Intent.ACTION_SEND);
                sendIntent.setType("text/plain");
                sendIntent.putExtra(Intent.EXTRA_TEXT, "99 Stories");
                startActivity(sendIntent);
                break;
            case R.id.bookmark:
                BOOKMARK_UNBOOKMARK_API();
                break;
        }
    }


    private void BOOKMARK_UNBOOKMARK_API() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add(Parameters.STORY_ID, story_id);
        RequestBody formBody = formBuilder.build();
        GetAsync mAsync = new GetAsync(this, All99StoriesApis.BOOKMARK_UNBOOKMARK_STORY_URL, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        Log.e("DATA", "data" + result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            if (is_bookmarked.equals("1")) {
                                Drawable img = getResources().getDrawable(R.drawable.bookmark_1);
                                bookmark.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                                is_bookmarked = "0";
                                bookmarks_count = String.valueOf(Integer.parseInt(bookmarks_count) - 1);
                                bookmark.setText(bookmarks_count);
                            } else {
                                Drawable img = getResources().getDrawable(R.drawable.bookmark_2);
                                bookmark.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                                is_bookmarked = "1";
                                bookmarks_count = String.valueOf(Integer.parseInt(bookmarks_count) + 1);
                                bookmark.setText(bookmarks_count);
                            }
                        } else {
                            util.showToast(context, jsonmainObject.getString("message"));
                        }

                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void LIKE_DISLIKE_STORY_API() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add(Parameters.STORY_ID, story_id);
        RequestBody formBody = formBuilder.build();
        GetAsync mAsync = new GetAsync(this, All99StoriesApis.LIKE_DISLIKE_STORY_URL, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        Log.e("DATA", "data" + result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            if (is_liked.equals("1")) {
                                Drawable img = getResources().getDrawable(R.drawable.like);
                                like.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                                is_liked = "0";
                                likes_count = String.valueOf(Integer.parseInt(likes_count) - 1);
                                like.setText(likes_count);
                            } else {
                                Drawable img = getResources().getDrawable(R.drawable.like);
                                like.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                                is_liked = "1";
                                likes_count = String.valueOf(Integer.parseInt(likes_count) + 1);
                                like.setText(likes_count);
                            }
                        } else {
                            util.showToast(context, jsonmainObject.getString("message"));
                        }

                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void DeleteAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage("Are you sure to delete story?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (ConnectivityReceiver.isConnected()) {
                            DELETE_USER_STORY_API();
                        } else {
                            util.showToast(context, util.internet_Connection_Error);
                        }

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void DELETE_USER_STORY_API() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add(Parameters.STORY_ID, story_id);
        RequestBody formBody = formBuilder.build();
        GetAsync mAsync = new GetAsync(this, All99StoriesApis.DELETE_USER_STORY_URL, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        Log.e("DATA", "data" + result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            finish();
                            util.showToast(context, jsonmainObject.getString("message"));
                        } else {
                            util.showToast(context, jsonmainObject.getString("message"));
                        }

                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    @Override
    protected void onResume() {
        super.onResume();
        VIEW_USER_STORY_API();
    }
}
