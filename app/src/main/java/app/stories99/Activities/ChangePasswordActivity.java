package app.stories99.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import app.stories99.MainActivity;
import app.stories99.R;
import app.stories99.parser.All99StoriesApis;
import app.stories99.util.ConnectivityReceiver;
import app.stories99.util.GetAsync;
import app.stories99.util.Parameters;
import app.stories99.util.util;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class ChangePasswordActivity extends AppCompatActivity {
    ChangePasswordActivity context;
    @BindView(R.id.old_password)
    EditText oldPassword;
    @BindView(R.id.new_password)
    EditText newPassword;
    @BindView(R.id.confirm_password)
    EditText confirmPassword;
    @BindView(R.id.Update)
    Button Update;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);
        setToolbar();
        context=ChangePasswordActivity.this;
    }

    private void CHANGE_PASSWORD_API() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add(Parameters.OLD_PASSWORD, oldPassword.getText().toString());
        formBuilder.add(Parameters.NEW_PASSWORD, newPassword.getText().toString());
        RequestBody formBody = formBuilder.build();
        GetAsync mAsync = new GetAsync(this, All99StoriesApis.CHANGE_PASSWORD_URL, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        Log.e("DATA", "data" + result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            finish();
                            util.showToast(context, jsonmainObject.getString("message"));
                        } else {

                            util.showToast(context, jsonmainObject.getString("message"));
                        }

                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void ChangePasswordTask() {
        if (ConnectivityReceiver.isConnected()) {
            if (oldPassword.getText().toString().isEmpty()) {
                oldPassword.requestFocus();
                util.showToast(context, "Please Enter Current Password");
            } else if (newPassword.getText().toString().isEmpty()) {
                newPassword.requestFocus();
                util.showToast(context, "Please Enter New Password");
            } else if (confirmPassword.getText().toString().isEmpty()) {
                confirmPassword.requestFocus();
                util.showToast(context, "Please Enter Confirm Password");
            } else if (!newPassword.getText().toString().equals(confirmPassword.getText().toString())) {
                confirmPassword.requestFocus();
                util.showToast(context, "Password Not Match");
            } else {
                CHANGE_PASSWORD_API();
            }
        } else {
            util.showToast(context, util.internet_Connection_Error);
        }
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText("Change Password");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @OnClick(R.id.Update)
    public void onViewClicked() {
        ChangePasswordTask();
    }
}
