package app.stories99.Activities;

import android.app.ProgressDialog;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import app.stories99.Adapters.CommentAdapter;
import app.stories99.Model.CommentModel;
import app.stories99.R;
import app.stories99.parser.All99StoriesApis;
import app.stories99.util.GetAsync;
import app.stories99.util.Parameters;
import app.stories99.util.util;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class CommentActivity extends AppCompatActivity {
    CommentActivity context;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.type_message)
    EditText typeMessage;
    @BindView(R.id.send_button)
    ImageView sendButton;

    private String story_id = "";

    CommentAdapter commentAdapter;

    private ArrayList<CommentModel> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        ButterKnife.bind(this);
        context = CommentActivity.this;
        story_id = getIntent().getStringExtra("story_id");
        setToolbar();

        STORY_COMMENTS_STORYAPI();
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText("Comments");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @OnClick(R.id.send_button)
    public void onViewClicked() {
        if (!typeMessage.getText().toString().isEmpty()) {
            COMMENT_STORY_API();
        }
    }

    private void STORY_COMMENTS_STORYAPI() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add(Parameters.STORY_ID, story_id);
        formBuilder.add(Parameters.COMMENT, typeMessage.getText().toString());
        RequestBody formBody = formBuilder.build();
        GetAsync mAsync = new GetAsync(this, All99StoriesApis.STORY_COMMENTS_STORY_URL, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                list = new ArrayList<>();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        Log.e("DATA", "data" + result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONArray comments = jsonmainObject.getJSONObject("body").getJSONArray("comments");

                            for (int i = 0; i < comments.length(); i++) {
                                JSONObject jsonObject = comments.getJSONObject(i);
                                CommentModel commentModel = new CommentModel();
                                commentModel.setId(jsonObject.getString("id"));
                                commentModel.setComment(jsonObject.getString("comment"));
                                commentModel.setCreated(jsonObject.getString("created"));
                                list.add(commentModel);
                            }
                            commentAdapter = new CommentAdapter(context, list);
                            myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                            myRecyclerView.setAdapter(commentAdapter);
                            myRecyclerView.scrollToPosition(commentAdapter.getItemCount() - 1);
                        } else {
                            util.showToast(context, jsonmainObject.getString("message"));
                        }

                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void COMMENT_STORY_API() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add(Parameters.STORY_ID, story_id);
        formBuilder.add(Parameters.COMMENT, typeMessage.getText().toString());
        RequestBody formBody = formBuilder.build();
        GetAsync mAsync = new GetAsync(this, All99StoriesApis.COMMENT_STORY_URL, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        Log.e("DATA", "data" + result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONArray comments = jsonmainObject.getJSONObject("body").getJSONArray("comment");

                            for (int i = 0; i < comments.length(); i++) {
                                JSONObject jsonObject = comments.getJSONObject(i);
                                CommentModel commentModel = new CommentModel();
                                commentModel.setId(jsonObject.getString("id"));
                                commentModel.setComment(jsonObject.getString("comment"));
                                commentModel.setCreated(jsonObject.getString("created"));
                                list.add(commentModel);
                            }
                            typeMessage.setText("");
                            if (list.size() == 0) {
                                commentAdapter = new CommentAdapter(context, list);
                                myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                                myRecyclerView.setAdapter(commentAdapter);
                            } else {
                                commentAdapter.notifyDataSetChanged();
                                myRecyclerView.scrollToPosition(commentAdapter.getItemCount() - 1);
                            }

                        } else {
                            util.showToast(context, jsonmainObject.getString("message"));
                        }

                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }
}
