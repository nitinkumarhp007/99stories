package app.stories99.Activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import app.stories99.Adapters.TrendingAdapter;
import app.stories99.Adapters.UserStorylistingAdapter;
import app.stories99.Model.StoryModel;
import app.stories99.R;
import app.stories99.parser.All99StoriesApis;
import app.stories99.util.GetAsync;
import app.stories99.util.GetAsyncGet;
import app.stories99.util.Parameters;
import app.stories99.util.util;
import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class UserStoriesActivity extends AppCompatActivity {
    UserStoriesActivity context;

    ArrayList<StoryModel> list;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;

    String user_id="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_stories);
        ButterKnife.bind(this);
        setToolbar();
        user_id=getIntent().getStringExtra("user_id");
        context = UserStoriesActivity.this;
        util.hideKeyboard(context);
        USER_STORIES_API();
    }

    private void USER_STORIES_API() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add(Parameters.USER_ID,user_id);
        RequestBody formBody = formBuilder.build();
        GetAsync mAsync = new GetAsync(context, All99StoriesApis.USER_STORIES_URL, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                list = new ArrayList<>();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        Log.e("DATA", "data" + result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONArray body = jsonmainObject.getJSONArray("body");

                            for (int i = 0; i < body.length(); i++) {
                                JSONObject jsonObject = body.getJSONObject(i);
                                StoryModel storyModel = new StoryModel();
                                storyModel.setStory_id(jsonObject.getString("story_id"));
                                storyModel.setImage(jsonObject.getString("story_image"));
                                storyModel.setTitle(jsonObject.getString("title"));
                                storyModel.setAuthor(jsonObject.getString("author"));
                                storyModel.setBookmarks_count(jsonObject.getString("bookmarks_count"));
                                storyModel.setComments_count(jsonObject.getString("comments_count"));
                                storyModel.setIs_bookmarked(jsonObject.getString("is_bookmarked"));
                                storyModel.setLikes_count(jsonObject.getString("likes_count"));
                                storyModel.setDescription(jsonObject.getString("description"));
                                storyModel.setSummary(jsonObject.getString("summary"));
                                list.add(storyModel);
                            }

                            UserStorylistingAdapter adapter = new UserStorylistingAdapter(context, list);
                            myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                            myRecyclerView.setAdapter(adapter);
                            //util.showToast(context, jsonmainObject.getString("message"));
                        } else {

                            util.showToast(context, jsonmainObject.getString("message"));
                        }

                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText("Stories");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
