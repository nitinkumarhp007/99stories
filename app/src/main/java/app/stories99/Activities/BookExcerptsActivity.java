package app.stories99.Activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import app.stories99.Adapters.BookExcerptsAdapter;
import app.stories99.Adapters.HomeAdapter;
import app.stories99.Adapters.HomeBookAdapter;
import app.stories99.Adapters.MyLibraryAdapter;
import app.stories99.Model.BookExcerptsModel;
import app.stories99.Model.CategoryModel;
import app.stories99.R;
import app.stories99.parser.All99StoriesApis;
import app.stories99.util.GetAsyncGet;
import app.stories99.util.SavePref;
import app.stories99.util.util;
import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class BookExcerptsActivity extends AppCompatActivity {
    BookExcerptsActivity context;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    ArrayList<BookExcerptsModel> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_excerpts);
        ButterKnife.bind(this);
        setToolbar();
        context = BookExcerptsActivity.this;
        EXCERPTS_LISTING_URLAPI();
    }

    private void EXCERPTS_LISTING_URLAPI() {
        SavePref savePref = new SavePref(context);
        Log.e("auth_key", savePref.getAUTH_KEY());
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        RequestBody formBody = formBuilder.build();
        GetAsyncGet mAsync = new GetAsyncGet(context, All99StoriesApis.EXCERPTS_LISTING_URL, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                list = new ArrayList<>();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        Log.e("DATA", "data" + result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONArray body = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < body.length(); i++) {
                                JSONObject jsonObject = body.getJSONObject(i);
                                BookExcerptsModel bookExcerptsModel = new BookExcerptsModel();
                                bookExcerptsModel.setDescription(jsonObject.getString("description"));
                                bookExcerptsModel.setId(jsonObject.getString("id"));
                                bookExcerptsModel.setLink(jsonObject.getString("link"));
                                bookExcerptsModel.setTitle(jsonObject.getString("title"));
                                list.add(bookExcerptsModel);
                            }
                            BookExcerptsAdapter adapter = new BookExcerptsAdapter(context,list);
                            myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                            myRecyclerView.setAdapter(adapter);
                        } else {

                            util.showToast(context, jsonmainObject.getString("message"));
                        }

                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText("Book Excerpts");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

}
