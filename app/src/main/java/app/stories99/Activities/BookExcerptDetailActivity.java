package app.stories99.Activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import app.stories99.Model.BookExcerptsModel;
import app.stories99.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BookExcerptDetailActivity extends AppCompatActivity {

    @BindView(R.id.text)
    TextView text;
    @BindView(R.id.buyButton)
    Button buyButton;
    ArrayList<BookExcerptsModel> list;
    int position = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_excerpt_detail);
        ButterKnife.bind(this);
        list = getIntent().getParcelableArrayListExtra("list");
        position = getIntent().getIntExtra("position", 0);
        text.setText(list.get(position).getDescription());
        setToolbar();
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText(list.get(position).getTitle());
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @OnClick(R.id.buyButton)
    public void onViewClicked() {
        if(!list.get(position).getLink().isEmpty())
        {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(list.get(position).getLink()));
            startActivity(browserIntent);
        }

    }
}
