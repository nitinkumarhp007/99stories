package app.stories99.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import app.stories99.MainActivity;
import app.stories99.R;
import app.stories99.parser.All99StoriesApis;
import app.stories99.util.ConnectivityReceiver;
import app.stories99.util.GetAsync;
import app.stories99.util.Parameters;
import app.stories99.util.SavePref;
import app.stories99.util.util;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class LoginActivity extends AppCompatActivity {
    LoginActivity context;
    SavePref savePref;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.login)
    Button login;
    @BindView(R.id.google)
    ImageView google;
    @BindView(R.id.signup)
    TextView signup;
    @BindView(R.id.forgotPassword)
    TextView forgotPassword;
    @BindView(R.id.Facebookbutton)
    LoginButton Facebookbutton;

    CallbackManager callbackManager;
    String fb_occupation = "", fb_username = "", fb_phone = "", fb_email = "", fb_image = "", social_id = "",
            fb_gender = "", fb_dob = "", fb_firstname = "", fb_lastname = "";
    ProgressDialog mDialog;

    private static final int RC_SIGN_IN = 0;
    private GoogleApiClient mGoogleApiClient;
    private GoogleSignInOptions googleSignInOptions;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        context = LoginActivity.this;
        savePref = new SavePref(context);
        mDialog=new ProgressDialog(context);


        callbackManager = CallbackManager.Factory.create();
        //googleplus login :)
        googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        //Initializing google api client
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions)
                .build();


        Facebookbutton.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);

        if (ConnectivityReceiver.isConnected()) {
            facebookLogin();
        } else {
            util.showToast(this, util.internet_Connection_Error);
        }

    }

    @OnClick({R.id.login, R.id.google, R.id.signup, R.id.forgotPassword})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.login:
                if (ConnectivityReceiver.isConnected()) {
                    if (email.getText().toString().isEmpty()) {
                        email.requestFocus();
                        util.showToast(context, "Please Enter Username");
                    } else if (password.getText().toString().isEmpty()) {
                        password.requestFocus();
                        util.showToast(context, "Please Enter Password");
                    } else {
                        SIGNIN_API();
                    }
                } else {
                    util.showToast(context, util.internet_Connection_Error);
                }
                break;
            case R.id.google:
                if (ConnectivityReceiver.isConnected()) {
                    signInWithGplus();
                } else {
                    util.showToast(this, util.internet_Connection_Error);
                }
                break;
            case R.id.signup:
                startActivity(new Intent(context, SignupActivity.class));
                break;
            case R.id.forgotPassword:
                startActivity(new Intent(context, ForgotPasswordActivity.class));
                break;
        }
    }

    private void SIGNIN_API() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add(Parameters.EMAIL_OR_USERNAME, email.getText().toString());
        formBuilder.add(Parameters.PASSWORD, password.getText().toString());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(this, All99StoriesApis.LOGIN_URL, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        Log.e("DATA", "data" + result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONObject jsonObjectBody = jsonmainObject.getJSONObject("body");
                            savePref.setEmail(jsonObjectBody.getString("email"));
                            savePref.setUserId(jsonObjectBody.getString("id"));
                            savePref.setName(jsonObjectBody.getString("username"));
                            savePref.setProfile_pic(jsonObjectBody.getString("profile_photo"));
                            savePref.setAUTH_KEY(jsonObjectBody.getString("authorization_key"));
                            Intent intent = new Intent(context, MainActivity.class);
                            startActivity(intent);
                            finish();
                            util.showToast(context, jsonmainObject.getString("message"));
                        } else {

                            util.showToast(context, jsonmainObject.getString("message"));
                        }

                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    /*social_id
            social_type
    email
            name
    photo
            device_type
    device_token*/
    private void SOCIAL_LOGIN_API(String type) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add(Parameters.SOCIAL_ID, social_id);
        formBuilder.add(Parameters.SOCIAL_TYPE, type);//1=fb,2=google
        formBuilder.add(Parameters.EMAIL, fb_email);
        formBuilder.add(Parameters.NAME, fb_firstname + " " + fb_lastname);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(this, All99StoriesApis.SOCIAL_LOGIN_URL, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        Log.e("DATA", "data" + result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONObject jsonObjectBody = jsonmainObject.getJSONObject("body");
                            savePref.setEmail(jsonObjectBody.getString("email"));
                            savePref.setUserId(jsonObjectBody.getString("id"));
                            savePref.setName(jsonObjectBody.getString("username"));
                            savePref.setProfile_pic(jsonObjectBody.getString("profile_photo"));
                            savePref.setAUTH_KEY(jsonObjectBody.getString("authorization_key"));
                            Intent intent = new Intent(context, MainActivity.class);
                            startActivity(intent);
                            finish();
                            util.showToast(context, jsonmainObject.getString("message"));
                        } else {

                            util.showToast(context, jsonmainObject.getString("message"));
                        }

                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void facebookLogin() {
        Facebookbutton.setReadPermissions(Arrays.asList("public_profile", "email", "user_birthday",
                "user_friends", "user_photos"));
        Facebookbutton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                mDialog.setMessage("please wait.....");
                mDialog.show();
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {

                                try {
                                    fb_email = object.getString("email");
                                    savePref.setEmail(fb_email);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    social_id = object.getString("id");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    fb_image = "http://graph.facebook.com/" + social_id + "/picture?type=large";
                                    //savePref.setImage(fb_image);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                try {
                                   /* fb_cover_image = "https://graph.facebook.com/"
                                            + socail_id + "?fields=cover&access_token=" + getCurrentAccessToken().getToken();*/

                                    JSONObject jsonObject = new JSONObject(response.toString());
                                    JSONObject sourceObj = jsonObject.getJSONObject("cover");
                                    String source = sourceObj.getString("source");
                                    // savePref.setCover_pic(source);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                try {
                                    fb_username = object.getString("name");
                                    savePref.setName(fb_username);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    fb_firstname = object.getString("first_name");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    fb_lastname = object.getString("last_name");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    fb_gender = object.getString("gender");
                                    fb_gender = fb_gender.substring(0, 1).toUpperCase()
                                            + fb_gender.substring(1, fb_gender.length());
                                    savePref.setGender(fb_gender);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    fb_dob = object.getString("birthday");
                                    //savePref.setDOB(fb_dob);

                                } catch (JSONException e) {
                                    e.printStackTrace();

                                }
                                SOCIAL_LOGIN_API("1");
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,first_name,last_name,email,gender,birthday");
                request.setParameters(parameters);
                request.executeAsync();
                mDialog.dismiss();
            }

            @Override
            public void onCancel() {
                mDialog.dismiss();
            }


            @Override
            public void onError(FacebookException error) {
                mDialog.dismiss();
            }

        });
    }

    //Google Plus Login
    //This function will option signing intent
    private void signInWithGplus() {
      //  mDialog.setMessage("please wait.....");
        //mDialog.show();
        //Creating an intent
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        //Starting intent for result
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    //For both google & fb login
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //for fb login
        callbackManager.onActivityResult(requestCode, resultCode, data);
        //for google+ login
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            //Calling a new function to handle signin
            handleSignInResult(result);

        }
    }

    //After the signing we are calling this function
    private void handleSignInResult(GoogleSignInResult result) {

        //If the login succeed
        if (result.isSuccess()) {
            //Getting google account
            GoogleSignInAccount acct = result.getSignInAccount();
            social_id = acct.getId();
            fb_firstname = acct.getGivenName();
            fb_email = acct.getEmail();
            Uri personPhoto = acct.getPhotoUrl();
            String username = acct.getDisplayName();
            SOCIAL_LOGIN_API("2");
        } else {
            //If login fails
            mDialog.show();
            Toast.makeText(this, "" + result.getStatus().toString(), Toast.LENGTH_LONG).show();
        }

    }
}
