package app.stories99.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;

import app.stories99.Model.CategoryModel;
import app.stories99.R;
import app.stories99.parser.All99StoriesApis;
import app.stories99.util.ConnectivityReceiver;
import app.stories99.util.GetAsync;
import app.stories99.util.GetAsyncGet;
import app.stories99.util.Parameters;
import app.stories99.util.SavePref;
import app.stories99.util.util;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class AddStoryActivity extends AppCompatActivity {
    AddStoryActivity context;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.category)
    TextView category;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.camera)
    ImageView camera;
    @BindView(R.id.image_layout)
    RelativeLayout imageLayout;
    @BindView(R.id.description)
    EditText description;
    @BindView(R.id.word_count)
    TextView wordCount;
    @BindView(R.id.upload)
    Button upload;
    @BindView(R.id.summery)
    EditText summery;
    @BindView(R.id.summery_word_count)
    TextView summeryWordCount;
    @BindView(R.id.description_layout)
    RelativeLayout descriptionLayout;
    @BindView(R.id.summary_layout)
    RelativeLayout summaryLayout;

    private int MEDIA_TYPE_GALLERY = 1;
    private int MEDIA_TYPE_CAPTURE = 2;
    private String selectedimage = "";

    ArrayList<CategoryModel> list;
    ArrayList<String> list_dialog = new ArrayList<>();
    String Category_id = "", story_id = "";
    boolean is_edit = false;

    String type = "";
    String title = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_story);
        ButterKnife.bind(this);
        context = AddStoryActivity.this;
        type = getIntent().getStringExtra("type");
        title = getIntent().getStringExtra("title");

        if (type.equals("1")) {
            name.setVisibility(View.GONE);
            category.setVisibility(View.GONE);
            imageLayout.setVisibility(View.GONE);
            summaryLayout.setVisibility(View.GONE);
            descriptionLayout.setVisibility(View.VISIBLE);
            description.setHint("Enter one liner");
        } else if (type.equals("2")) {
            category.setVisibility(View.GONE);
            imageLayout.setVisibility(View.GONE);
            summaryLayout.setVisibility(View.GONE);
            descriptionLayout.setVisibility(View.VISIBLE);
            description.setHint("Enter a poem");
            name.setHint("Enter poem name");
        } else if (type.equals("3")) {
            name.setVisibility(View.GONE);
            category.setVisibility(View.GONE);
            summaryLayout.setVisibility(View.GONE);
            descriptionLayout.setVisibility(View.GONE);
        }

        is_edit = getIntent().getBooleanExtra("is_edit", false);

        if (is_edit) {
            story_id = getIntent().getStringExtra("story_id");

            name.setText(getIntent().getStringExtra("title"));
            category.setText(getIntent().getStringExtra("category_title"));
            description.setText(getIntent().getStringExtra("description"));
            summery.setText(getIntent().getStringExtra("summary"));

            Category_id = getIntent().getStringExtra("category_id");

            Glide.with(context).load(getIntent().getStringExtra("story_image")).into(image);


            summery.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    Log.e("text_chnage", "beforeTextChanged");
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    Log.e("text_chnage", "onTextChanged");
                    int words_written = s.toString().length();
                    summeryWordCount.setText(String.valueOf(words_written));
                }

                @Override
                public void afterTextChanged(Editable s) {
                    Log.e("text_chnage", "afterTextChanged");
                }
            });


        }

        setToolbar();
        if (type.equals("0"))
            GET_ALL_CATEGORIES_API();
    }

    private void AddPostTask() {
        if (ConnectivityReceiver.isConnected()) {
            if (is_edit) {
                if (type.equals("0")) {
                    if (name.getText().toString().isEmpty()) {
                        name.requestFocus();
                        util.showToast(context, "Please Enter Story Name");
                    } else if (summery.getText().toString().isEmpty()) {
                        summery.requestFocus();
                        util.showToast(context, "Please Enter Story Summary");
                    } else if (description.getText().toString().isEmpty()) {
                        description.requestFocus();
                        util.showToast(context, "Please Enter Story Description");
                    } else {
                        POST_USER_STORY_API();
                    }
                } else if (type.equals("1")) {
                    if (description.getText().toString().isEmpty()) {
                        description.requestFocus();
                        util.showToast(context, "Please Enter One liner");
                    } else {
                        POST_USER_STORY_API();
                    }
                } else if (type.equals("2")) {
                    if (name.getText().toString().isEmpty()) {
                        name.requestFocus();
                        util.showToast(context, "Please Enter Poem Name");
                    } else if (description.getText().toString().isEmpty()) {
                        description.requestFocus();
                        util.showToast(context, "Please Enter a Poem");
                    } else {
                        POST_USER_STORY_API();
                    }

                } else if (type.equals("3")) {
                    POST_USER_STORY_API();
                }

            } else {

                if (type.equals("0")) {
                    if (name.getText().toString().isEmpty()) {
                        name.requestFocus();
                        util.showToast(context, "Please Enter Story Name");
                    } else if (Category_id.isEmpty()) {
                        util.showToast(context, "Please Select Category");
                    } else if (selectedimage.isEmpty()) {
                        util.showToast(context, "Please Select Story Image");
                    } else if (summery.getText().toString().isEmpty()) {
                        summery.requestFocus();
                        util.showToast(context, "Please Enter Story Summary");
                    } else if (description.getText().toString().isEmpty()) {
                        description.requestFocus();
                        util.showToast(context, "Please Enter Story Description");
                    } else {
                        POST_USER_STORY_API();
                    }
                } else if (type.equals("1")) {
                    if (description.getText().toString().isEmpty()) {
                        description.requestFocus();
                        util.showToast(context, "Please Enter One liner");
                    } else {
                        POST_USER_STORY_API();
                    }
                } else if (type.equals("2")) {
                    if (name.getText().toString().isEmpty()) {
                        name.requestFocus();
                        util.showToast(context, "Please Enter Poem Name");
                    } else if (description.getText().toString().isEmpty()) {
                        description.requestFocus();
                        util.showToast(context, "Please Enter a Poem");
                    } else {
                        POST_USER_STORY_API();
                    }

                } else if (type.equals("3")) {
                    if (selectedimage.isEmpty()) {
                        util.showToast(context, "Please Select Image");
                    } else {
                        POST_USER_STORY_API();
                    }
                }

            }

        } else {
            util.showToast(context, util.internet_Connection_Error);
        }
    }

    @OnClick({R.id.category, R.id.image_layout, R.id.upload})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.category:
                categoryDialog();
                break;
            case R.id.image_layout:
                options();
                break;
            case R.id.upload:
                AddPostTask();
                break;
        }
    }

    private void POST_USER_STORY_API() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        if (!selectedimage.equalsIgnoreCase("")) {
            final MediaType MEDIA_TYPE = selectedimage.endsWith("png") ?
                    MediaType.parse("image/png") : MediaType.parse("image/jpeg");

            File file = new File(selectedimage);
            formBuilder.addFormDataPart("image", file.getName(), RequestBody.create(MEDIA_TYPE, file));
        }
        if (is_edit) {
            formBuilder.addFormDataPart(Parameters.STORY_ID, story_id);
        }
        formBuilder.addFormDataPart(Parameters.TITLE, name.getText().toString());
        formBuilder.addFormDataPart(Parameters.CATEGORY_ID, Category_id);
        formBuilder.addFormDataPart(Parameters.DESCRIPTION, description.getText().toString());
        formBuilder.addFormDataPart(Parameters.SUMMARY, summery.getText().toString());
        formBuilder.addFormDataPart(Parameters.TYPE, type);
        RequestBody formBody = formBuilder.build();
        GetAsync mAsync = new GetAsync(context, All99StoriesApis.POST_USER_STORY_URL, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {
                            util.showToast(context, jsonObject.getString("message"));
                            Intent intent = new Intent();
                            setResult(Activity.RESULT_OK, intent);
                            finish();
                            util.hideKeyboard(context);
                        } else {
                            util.showToast(context, jsonObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    private void categoryDialog() {
        final CharSequence[] Animals = list_dialog.toArray(new String[list_dialog.size()]);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle("Select Category");
        dialogBuilder.setItems(Animals, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                Category_id = list.get(item).getCategory_id();
                category.setText(list.get(item).getTitle());
            }
        });
        //Create alert dialog object via builder
        AlertDialog alertDialogObject = dialogBuilder.create();
        //Show the dialog
        alertDialogObject.show();
    }

    private void GET_ALL_CATEGORIES_API() {
        SavePref savePref = new SavePref(context);
        Log.e("auth_key", savePref.getAUTH_KEY());
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        RequestBody formBody = formBuilder.build();
        GetAsyncGet mAsync = new GetAsyncGet(context, All99StoriesApis.GET_ALL_CATEGORIES_URL, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                list = new ArrayList<>();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        Log.e("DATA", "data" + result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONArray body = jsonmainObject.getJSONArray("body");

                            for (int i = 0; i < body.length(); i++) {
                                JSONObject jsonObject = body.getJSONObject(i);
                                CategoryModel categoryModel = new CategoryModel();
                                categoryModel.setCategory_id(jsonObject.getString("id"));
                                categoryModel.setImage(jsonObject.getString("image"));
                                categoryModel.setTitle(jsonObject.getString("title"));
                                list.add(categoryModel);
                                list_dialog.add(jsonObject.getString("title"));
                            }
                        } else {

                            util.showToast(context, jsonmainObject.getString("message"));
                        }

                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title_ = (TextView) toolbar.findViewById(R.id.title);
        title_.setText(title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    public static class Item {
        public final String text;
        public final int icon;

        public Item(String text, Integer icon) {
            this.text = text;
            this.icon = icon;
        }

        @Override
        public String toString() {
            return text;
        }
    }

    private void options() {
        final SignupActivity.Item[] items = {
                new SignupActivity.Item(getResources().getString(R.string.Camera), android.R.drawable.ic_menu_camera),
                new SignupActivity.Item(getResources().getString(R.string.Choose_from_Gallery), android.R.drawable.ic_menu_gallery),
                new SignupActivity.Item(getResources().getString(R.string.Cancel), android.R.drawable.ic_notification_clear_all),
        };
        ListAdapter adapter = new ArrayAdapter(
                this,
                android.R.layout.select_dialog_item,
                android.R.id.text1,
                items) {
            public View getView(int position, View convertView, ViewGroup parent) {
                //Use super class to create the View
                View v = super.getView(position, convertView, parent);
                TextView tv = (TextView) v.findViewById(android.R.id.text1);
                tv.setTextSize(14f);
                //Put the image on the TextView
                tv.setCompoundDrawablesWithIntrinsicBounds(items[position].icon, 0, 0, 0);

                //Add margin between image and text (support various screen densities)
                int dp5 = (int) (5 * getResources().getDisplayMetrics().density + 0.5f);
                tv.setCompoundDrawablePadding(dp5);

                return v;
            }
        };


        new android.support.v7.app.AlertDialog.Builder(this)
                .setTitle("Select Image!")
                .setAdapter(adapter, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        if (item == 0) {
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, MEDIA_TYPE_CAPTURE);
                            dialog.dismiss();
                        } else if (item == 1) {
                            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                            photoPickerIntent.setType("image/*");
                            startActivityForResult(photoPickerIntent, MEDIA_TYPE_GALLERY);
                            dialog.dismiss();
                        } else {
                            dialog.dismiss();
                        }
                    }
                }).show();
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(),
                inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MEDIA_TYPE_GALLERY) {
            if (resultCode == RESULT_OK) {

                selectedimage = util.getAbsolutePath(context, data.getData());
                Glide.with(context).load(selectedimage).into(image);

            } else if (resultCode == RESULT_CANCELED) {
                /*Toast.makeText(getApplicationContext(), "Cancelled",
                        Toast.LENGTH_SHORT).show();*/
            }
        } else if (requestCode == MEDIA_TYPE_CAPTURE) {
            if (resultCode == RESULT_OK) {
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                Uri tempUri = getImageUri(getApplicationContext(), photo);
                File finalFile = new File(getRealPathFromURI(tempUri));

                selectedimage = finalFile.toString();
                Glide.with(this).load(selectedimage).into(image);

            } else if (resultCode == RESULT_CANCELED) {
                /*Toast.makeText(getApplicationContext(), "Cancelled",
                        Toast.LENGTH_SHORT).show();*/
            }
        }
    }

}