package app.stories99.Activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import app.stories99.R;
import app.stories99.parser.All99StoriesApis;
import app.stories99.util.GetAsync;
import app.stories99.util.Parameters;
import app.stories99.util.util;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class ReportStoryActivity extends AppCompatActivity {

    @BindView(R.id.text)
    EditText text;
    @BindView(R.id.add)
    Button add;

    ReportStoryActivity context;

    String story_id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_story);
        ButterKnife.bind(this);

        context = ReportStoryActivity.this;
        story_id = getIntent().getStringExtra("story_id");
        util.hideKeyboard(context);
        setToolbar();
    }

    @OnClick(R.id.add)
    public void onViewClicked() {
        if (text.getText().toString().isEmpty()) {
            Toast.makeText(this, "Please Enter Reason", Toast.LENGTH_SHORT).show();
        } else {
            REPORT_STORYAPI(text.getText().toString());
        }
    }

    private void REPORT_STORYAPI(String text) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add(Parameters.REASON, text);
        formBuilder.add(Parameters.STORY_ID, story_id);
        RequestBody formBody = formBuilder.build();
        GetAsync mAsync = new GetAsync(context, All99StoriesApis.REPORT_STORY, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        Log.e("DATA", "data" + result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            Toast.makeText(ReportStoryActivity.this, jsonmainObject.getString("message"), Toast.LENGTH_SHORT).show();
                            finish();
                        } else {

                            util.showToast(context, jsonmainObject.getString("message"));
                        }

                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText("Report Abuse");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            util.hideKeyboard(context);
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        util.hideKeyboard(context);
    }
}
