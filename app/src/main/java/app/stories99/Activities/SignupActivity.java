package app.stories99.Activities;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import app.stories99.R;
import app.stories99.parser.All99StoriesApis;
import app.stories99.util.ConnectivityReceiver;
import app.stories99.util.GetAsync;
import app.stories99.util.Parameters;
import app.stories99.util.util;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class SignupActivity extends AppCompatActivity {
    SignupActivity context;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.username)
    EditText username;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.confirm_password)
    EditText confirmPassword;
    @BindView(R.id.signup)
    Button signup;
    @BindView(R.id.signin)
    TextView signin;
    @BindView(R.id.termCheck)
    CheckBox termCheck;

    Uri fileUri;
    private int MEDIA_TYPE_GALLERY = 1;
    private int MEDIA_TYPE_CAPTURE = 2;
    private String selectedimage = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);
        context = SignupActivity.this;
    }

    @OnClick({R.id.image, R.id.signin, R.id.signup})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image:
                options();
                break;
            case R.id.signup:
                SignupTask();
                break;
            case R.id.signin:
                startActivity(new Intent(context, LoginActivity.class));
                break;
        }
    }

    private void SignupTask() {
        if (ConnectivityReceiver.isConnected()) {
            if (selectedimage.isEmpty()) {
                util.showToast(context, "Please Select Profile photo");
            } else if (username.getText().toString().isEmpty()) {
                username.requestFocus();
                util.showToast(context, "Please Enter Username");
            } else if (email.getText().toString().isEmpty()) {
                email.requestFocus();
                util.showToast(context, "Please Enter Email-Address");
            } else if (!util.isValidEmail(email.getText().toString())) {
                email.requestFocus();
                util.showToast(context, "Please Enter Vaild Email-Address");
            } else if (password.getText().toString().isEmpty()) {
                password.requestFocus();
                util.showToast(context, "Please Enter Password");
            } else if (confirmPassword.getText().toString().isEmpty()) {
                confirmPassword.requestFocus();
                util.showToast(context, "Please Confirm Password");
            } else if (!confirmPassword.getText().toString().equals(password.getText().toString())) {
                confirmPassword.requestFocus();
                util.showToast(context, "Password Not Match");
            } else if (!termCheck.isChecked()) {
                util.showToast(context, getResources().getString(R.string.PleaseAcceptTerm));
            } else {
                SIGNUP_API();
            }
        } else {
            util.showToast(context, util.internet_Connection_Error);
        }
    }

    /*name
            username
    email
            password
    profile_photo*/
    private void SIGNUP_API() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        if (!selectedimage.equalsIgnoreCase("")) {
            final MediaType MEDIA_TYPE = selectedimage.endsWith("png") ?
                    MediaType.parse("image/png") : MediaType.parse("image/jpeg");

            File file = new File(selectedimage);
            formBuilder.addFormDataPart(Parameters.PROFILE_PHOTO, file.getName(), RequestBody.create(MEDIA_TYPE, file));
        }
        formBuilder.addFormDataPart(Parameters.EMAIL, email.getText().toString());
        formBuilder.addFormDataPart(Parameters.USERNAME, username.getText().toString());
        formBuilder.addFormDataPart(Parameters.NAME, username.getText().toString());
        formBuilder.addFormDataPart(Parameters.PASSWORD, password.getText().toString());
        RequestBody formBody = formBuilder.build();
        GetAsync mAsync = new GetAsync(context, All99StoriesApis.SIGNUP_URL, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {
                            util.showToast(context, jsonObject.getString("message"));
                            Intent intent = new Intent(context, LoginActivity.class);
                            startActivity(intent);
                            finish();
                            util.hideKeyboard(context);
                        } else {
                            util.showToast(context, jsonObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    public static class Item {
        public final String text;
        public final int icon;

        public Item(String text, Integer icon) {
            this.text = text;
            this.icon = icon;
        }

        @Override
        public String toString() {
            return text;
        }
    }

    private void options() {
        final Item[] items = {
                new Item(getResources().getString(R.string.Camera), android.R.drawable.ic_menu_camera),
                new Item(getResources().getString(R.string.Choose_from_Gallery), android.R.drawable.ic_menu_gallery),
                new Item(getResources().getString(R.string.Cancel), android.R.drawable.ic_notification_clear_all),
        };
        ListAdapter adapter = new ArrayAdapter(
                this,
                android.R.layout.select_dialog_item,
                android.R.id.text1,
                items) {
            public View getView(int position, View convertView, ViewGroup parent) {
                //Use super class to create the View
                View v = super.getView(position, convertView, parent);
                TextView tv = (TextView) v.findViewById(android.R.id.text1);
                tv.setTextSize(14f);
                //Put the image on the TextView
                tv.setCompoundDrawablesWithIntrinsicBounds(items[position].icon, 0, 0, 0);

                //Add margin between image and text (support various screen densities)
                int dp5 = (int) (5 * getResources().getDisplayMetrics().density + 0.5f);
                tv.setCompoundDrawablePadding(dp5);

                return v;
            }
        };


        new AlertDialog.Builder(this)
                .setTitle("Select Image!")
                .setAdapter(adapter, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        if (item == 0) {
                            /*Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, MEDIA_TYPE_CAPTURE);*/
                            captureImage();
                            dialog.dismiss();
                        } else if (item == 1) {
                            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                            photoPickerIntent.setType("image/*");
                            startActivityForResult(photoPickerIntent, MEDIA_TYPE_GALLERY);
                            dialog.dismiss();
                        } else {
                            dialog.dismiss();
                        }
                    }
                }).show();
    }
    private void captureImage() {
        //useful in android naught 7.0
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getPackageManager()) != null) {
            ContentValues values = new ContentValues(1);
            values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg");
            fileUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            startActivityForResult(intent, MEDIA_TYPE_CAPTURE);
        } else {
            //Toast.makeText(mainActivity, "No Camera Found", Toast.LENGTH_SHORT).show();
        }
    }
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(),
                inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }
    private void bitmap(Uri resultUri) {
        Bitmap bitmap = null;
        try {
            bitmap = (MediaStore.Images.Media.getBitmap(getContentResolver(), resultUri));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MEDIA_TYPE_GALLERY) {
            if (resultCode == RESULT_OK) {

                selectedimage = util.getAbsolutePath(context, data.getData());
                Glide.with(context).load(selectedimage).into(image);

            } else if (resultCode == RESULT_CANCELED) {
                /*Toast.makeText(getApplicationContext(), "Cancelled",
                        Toast.LENGTH_SHORT).show();*/
            }
        } else if (requestCode == MEDIA_TYPE_CAPTURE) {
            if (resultCode == RESULT_OK) {
                bitmap(fileUri);
                selectedimage = util.getPath(this, fileUri);
                Glide.with(this).load(selectedimage).into(image);

            } else if (resultCode == RESULT_CANCELED) {
                /*Toast.makeText(getApplicationContext(), "Cancelled",
                        Toast.LENGTH_SHORT).show();*/
            }
        }
    }

}
