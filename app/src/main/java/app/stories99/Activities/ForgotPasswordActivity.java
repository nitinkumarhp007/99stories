package app.stories99.Activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import app.stories99.R;
import app.stories99.parser.All99StoriesApis;
import app.stories99.util.ConnectivityReceiver;
import app.stories99.util.GetAsync;
import app.stories99.util.Parameters;
import app.stories99.util.util;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class ForgotPasswordActivity extends AppCompatActivity {
    ForgotPasswordActivity context;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.Send)
    Button Send;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);
        context = ForgotPasswordActivity.this;
    }

    private void FORGOT_PASSWORD_API() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add(Parameters.EMAIL, email.getText().toString());
        RequestBody formBody = formBuilder.build();
        GetAsync mAsync = new GetAsync(this, All99StoriesApis.FORGOT_PASSWORD_URL, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        Log.e("DATA", "data" + result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            finish();
                            util.showToast(context, jsonmainObject.getString("message"));
                        } else {

                            util.showToast(context, jsonmainObject.getString("message"));
                        }

                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    @OnClick(R.id.Send)
    public void onViewClicked() {

        if (ConnectivityReceiver.isConnected()) {
            if (email.getText().toString().isEmpty()) {
                email.requestFocus();
                util.showToast(context, "Please Enter Email-Address");
            } else if (!util.isValidEmail(email.getText().toString())) {
                email.requestFocus();
                util.showToast(context, "Please Enter Vaild Email-Address");
            } else {
                FORGOT_PASSWORD_API();
            }
        } else {
            util.showToast(context, util.internet_Connection_Error);
        }

    }
}
