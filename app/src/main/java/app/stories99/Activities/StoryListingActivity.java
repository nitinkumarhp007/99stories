package app.stories99.Activities;

import android.app.ProgressDialog;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import app.stories99.Adapters.HomeAdapter;
import app.stories99.Adapters.StorylistingAdapter;
import app.stories99.Model.CategoryModel;
import app.stories99.Model.StoryModel;
import app.stories99.R;
import app.stories99.parser.All99StoriesApis;
import app.stories99.util.GetAsync;
import app.stories99.util.GetAsyncGet;
import app.stories99.util.Parameters;
import app.stories99.util.util;
import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class StoryListingActivity extends AppCompatActivity {
    StoryListingActivity context;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    String category_id = "", category_name = "";
    private ArrayList<StoryModel> list;

    StorylistingAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_story_listing);
        ButterKnife.bind(this);
        context = StoryListingActivity.this;
        category_id = getIntent().getStringExtra("category_id");
        category_name = getIntent().getStringExtra("category_name");
        setToolbar();
        GET_CATEGORY_STORY_API();
    }

    private void GET_CATEGORY_STORY_API() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add(Parameters.CATEGORY_ID, category_id);
        RequestBody formBody = formBuilder.build();
        GetAsync mAsync = new GetAsync(context, All99StoriesApis.GET_CATEGORY_STORY_URL, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                list = new ArrayList<>();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        Log.e("DATA", "data" + result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONArray body = jsonmainObject.getJSONArray("body");

                            for (int i = 0; i < body.length(); i++) {
                                JSONObject jsonObject = body.getJSONObject(i);
                                StoryModel storyModel = new StoryModel();
                                storyModel.setStory_id(jsonObject.getString("id"));
                                storyModel.setImage(jsonObject.getString("image"));
                                storyModel.setTitle(jsonObject.getString("title"));
                                storyModel.setAuthor(jsonObject.getString("author"));
                                storyModel.setBookmarks_count(jsonObject.getString("bookmarks_count"));
                                storyModel.setComments_count(jsonObject.getString("comments_count"));
                                storyModel.setIs_bookmarked(jsonObject.getString("is_bookmarked"));
                                storyModel.setLikes_count(jsonObject.getString("likes_count"));
                                storyModel.setIs_like(jsonObject.getString("is_liked"));
                                storyModel.setSummary(jsonObject.getString("summary"));
                                storyModel.setType(jsonObject.optString("type"));
                                storyModel.setDescription(jsonObject.getString("description"));
                                list.add(storyModel);
                            }
                            adapter = new StorylistingAdapter(context, list);
                            myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                            myRecyclerView.setAdapter(adapter);
                         //   util.showToast(context, jsonmainObject.getString("message"));
                        } else {

                            util.showToast(context, jsonmainObject.getString("message"));
                        }

                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }



    public void LIKE_DISLIKE_STORY_API(final int position) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add(Parameters.STORY_ID, list.get(position).getStory_id());
        RequestBody formBody = formBuilder.build();
        GetAsync mAsync = new GetAsync(this, All99StoriesApis.LIKE_DISLIKE_STORY_URL, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        Log.e("DATA", "data" + result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            if (list.get(position).getIs_like().equals("1")) {
                                list.get(position).setIs_like("0");
                                list.get(position).setLikes_count(String.valueOf(Integer.parseInt(list.get(position).getLikes_count()) - 1));
                            } else {
                                list.get(position).setIs_like("1");
                                list.get(position).setLikes_count(String.valueOf(Integer.parseInt(list.get(position).getLikes_count()) + 1));
                            }
                            adapter.notifyDataSetChanged();
                        } else {
                            util.showToast(context, jsonmainObject.getString("message"));
                        }

                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void BOOKMARK_UNBOOKMARK_STORY_API(final int position) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add(Parameters.STORY_ID, list.get(position).getStory_id());
        RequestBody formBody = formBuilder.build();
        GetAsync mAsync = new GetAsync(this, All99StoriesApis.BOOKMARK_UNBOOKMARK_STORY_URL, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        Log.e("DATA", "data" + result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            if (list.get(position).getIs_bookmarked().equals("1")) {
                                list.get(position).setIs_bookmarked("0");
                                list.get(position).setBookmarks_count(String.valueOf(Integer.parseInt(list.get(position).getBookmarks_count()) - 1));
                            } else {
                                list.get(position).setIs_bookmarked("1");
                                list.get(position).setBookmarks_count(String.valueOf(Integer.parseInt(list.get(position).getBookmarks_count()) + 1));
                            }
                            adapter.notifyDataSetChanged();
                        } else {
                            util.showToast(context, jsonmainObject.getString("message"));
                        }

                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText(category_name);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

}
